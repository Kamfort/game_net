extends KinematicBody2D

var FLAP = 80
var MAXFALLSPEED = 200
var GRAVITY = 700
var wall = preload("res://wall_node.tscn")

var motion = Vector2()

func _physics_process(delta):
	motion.y += GRAVITY*delta
	if motion.y > MAXFALLSPEED:
		motion.y = MAXFALLSPEED
	
	if Input.is_action_pressed("player_jump"):
		motion.y -= FLAP
	
	motion = move_and_slide(motion, Vector2.UP)

func wall_reset():
	var wall_instance = wall.instance()
	wall_instance.position = Vector2(200,rand_range(50,400))
	get_parent().call_deferred("add_child", wall_instance)

func _on_Resseter_body_entered(body):
	if body.name == "Walls":
		body.queue_free()
		wall_reset()
